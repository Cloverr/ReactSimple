import React from 'react';
import ReactDOM from 'react-dom';

import Home from './js/components/home/home.js';


ReactDOM.render(<Home />, document.getElementById('root'));
